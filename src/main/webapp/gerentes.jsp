<%-- 
    Document   : gerentes
    Created on : Jan 5, 2017, 1:32:18 AM
    Author     : fabio
--%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP List Users Records</title>
</head>

   
    <sql:query var="listUsers"   dataSource="jdbc/myoracle">
      SELECT * FROM ORIGINAL.GERENTES
    </sql:query>
     
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2>List of managers</h2></caption>
            <tr>
                <th>ID</th>
                <th>Nome</th>

            </tr>
            <c:forEach var="user" items="${listUsers.rows}">
                <tr>
                    <td><c:out value="${user.id}" /></td>
                    <td><c:out value="${user.nome}" /></td>
                </tr>
            </c:forEach>
        </table>
    </div>

